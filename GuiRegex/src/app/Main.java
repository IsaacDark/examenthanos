package app;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Principal frame = new Principal();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
