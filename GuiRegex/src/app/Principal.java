/*
 * Created by JFormDesigner on Wed Apr 28 19:29:41 CDT 2021
 */

package app;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author unknown
 */
public class Principal extends JFrame {
    private Editor edit;
    public Principal() {
        initComponents();
    }
    boolean flag = false;
    private JFileChooser fcrSelector;
    private File f;
    private FileWriter fw;

    private void btnNuevoActionPerformed(ActionEvent e) {
        if(!flag){
            int op = fcrSelector.showSaveDialog(this);
            if(op==JFileChooser.APPROVE_OPTION){
                f = fcrSelector.getSelectedFile();
                if(!f.exists()){
                    try{
                        f.createNewFile();
                        edit = new Editor(f);
                        tbpContenedorFuente.add(edit,f.getName());
                        flag = true;
                    }catch (IOException ex){
                        System.out.println("Error "+ ex.getMessage());
                    }
                }else{
                    JOptionPane.showMessageDialog(this,"Archivo ya existe");
                }
            }
        }else{
            JOptionPane.showMessageDialog(null,"Primero cierra el archivo");
        }
    }

    private void btnAbrirActionPerformed(ActionEvent e) {
        if(!flag){
            FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos .m","m");
            fcrSelector.setFileFilter(filtro);
            int op = fcrSelector.showOpenDialog(this);
            if(op==JFileChooser.APPROVE_OPTION){
                f = fcrSelector.getSelectedFile();
                if(f.canRead()) {
                    if (f.getName().endsWith("m")) {

                        edit = new Editor(f);
                        try{
                            BufferedReader leer = new BufferedReader(new FileReader(f));
                            String linea = leer.readLine();
                            int cont = 1;
                            while(linea!=null){
                                edit.jtaContenido.append(linea+"\n");
                                linea = leer.readLine();
                            }
                            tbpContenedorFuente.add(edit,f.getName());
                            flag = true;
                        }catch (Exception ex) {
                            System.out.println("No se pudo leer el archivo");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Archivo no compatible");
                    }
                }
            }
        }else{
            JOptionPane.showMessageDialog(null,"Primero cierra el archivo");
        }
    }

    private void btnGuardarActionPerformed(ActionEvent e) {
        if(!(f==null)){
            try {
                fw= new FileWriter(f.getAbsolutePath());
                fw.write(edit.jtaContenido.getText());
                fw.close();
                JOptionPane.showMessageDialog(null,"Guardado correctamente.");
                flag = false;
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }else{
            JOptionPane.showMessageDialog(null,"No hay archivos abiertos");
        }
    }

    private void btnCerrarActionPerformed(ActionEvent e) {
        if(!(f==null)){
            tbpContenedorFuente.remove(edit);
            flag = false;
            JOptionPane.showMessageDialog(null,"Archivo Cerrado correctamente");
            f = null;
        }else{
            JOptionPane.showMessageDialog(null,"No hay archivos abiertos");
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        toolBar1 = new JToolBar();
        btnAbrir = new JButton();
        btnNuevo = new JButton();
        btnGuardar = new JButton();
        btnLexico = new JButton();
        btnCerrar = new JButton();
        splitPane1 = new JSplitPane();
        tbpContenedorFuente = new JTabbedPane();
        tbpContenedorLexico = new JTabbedPane();
        fcrSelector = new JFileChooser();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== toolBar1 ========
        {

            //---- btnAbrir ----
            btnAbrir.setText("Abrir");
            btnAbrir.addActionListener(e -> btnAbrirActionPerformed(e));
            toolBar1.add(btnAbrir);

            //---- btnNuevo ----
            btnNuevo.setText("Nuevo");
            btnNuevo.addActionListener(e -> btnNuevoActionPerformed(e));
            toolBar1.add(btnNuevo);

            //---- btnGuardar ----
            btnGuardar.setText("Guardar");
            btnGuardar.addActionListener(e -> btnGuardarActionPerformed(e));
            toolBar1.add(btnGuardar);

            //---- btnLexico ----
            btnLexico.setText("Analizador L\u00e9xico");
            toolBar1.add(btnLexico);

            //---- btnCerrar ----
            btnCerrar.setText("Cerrar");
            btnCerrar.addActionListener(e -> btnCerrarActionPerformed(e));
            toolBar1.add(btnCerrar);
        }
        contentPane.add(toolBar1, BorderLayout.NORTH);

        //======== splitPane1 ========
        {
            splitPane1.setDividerLocation(300);
            splitPane1.setLeftComponent(tbpContenedorFuente);
            splitPane1.setRightComponent(tbpContenedorLexico);
        }
        contentPane.add(splitPane1, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JToolBar toolBar1;
    private JButton btnAbrir;
    private JButton btnNuevo;
    private JButton btnGuardar;
    private JButton btnLexico;
    private JButton btnCerrar;
    private JSplitPane splitPane1;
    private JTabbedPane tbpContenedorFuente;
    private JTabbedPane tbpContenedorLexico;
}
